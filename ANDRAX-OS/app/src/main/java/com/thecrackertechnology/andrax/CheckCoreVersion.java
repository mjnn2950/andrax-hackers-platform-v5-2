package com.thecrackertechnology.andrax;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

public class CheckCoreVersion extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.checkcoreversion);

    }

    @Override
    public void onPause() {
        super.onPause();
        finish();
    }

}
